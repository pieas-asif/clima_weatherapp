import 'package:Weether/screens/city_screen.dart';
import 'package:Weether/services/weather.dart';
import 'package:flutter/material.dart';
import 'package:Weether/utilities/constants.dart';
import 'dart:ui';

import 'package:weather_icons/weather_icons.dart';

class LocationScreen extends StatefulWidget {
  LocationScreen({@required this.locationData});
  final locationData;

  @override
  _LocationScreenState createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  WeatherModel weather = WeatherModel();
  int temperature;
  String description;
  String weatherMessage;
  String condition;
  String cityName;
  IconData weatherIcon;
  String pressure;
  String humidity;
  String minTemp;
  String maxTemp;
  String windSpeed;

  String titleCase(String text) {
    if (text.length <= 1) return text.toUpperCase();
    var words = text.split(' ');
    var capitalized = words.map((word) {
      var first = word.substring(0, 1).toUpperCase();
      var rest = word.substring(1);
      return '$first$rest';
    });
    return capitalized.join(' ');
  }

  @override
  void initState() {
    super.initState();
    updateUI(widget.locationData);
  }

  void updateUI(dynamic weatherData) {
    if (weatherData == null) {
      temperature = 404;
      weatherIcon = Icons.error;
      description = "No Data";
      cityName = "🌃";
      pressure = "🌡";
      humidity = "💦";
      minTemp = "🥶";
      maxTemp = "🥵";
      windSpeed = "💨";
      return;
    }
    var _t = weatherData["main"]["temp"];
    temperature = _t.toInt();
    description = weatherData["weather"][0]["description"];
    var _conditionId = weatherData["weather"][0]["id"];
    weatherIcon = weather.getWeatherIcon(_conditionId);
    cityName = weatherData["name"];
    _t = weatherData["main"]["pressure"];
    pressure = _t.toString();
    _t = weatherData["main"]["humidity"];
    humidity = _t.toString();
    _t = weatherData["main"]["temp_min"];
    minTemp = _t.toString();
    _t = weatherData["main"]["temp_max"];
    maxTemp = _t.toString();
    _t = weatherData["wind"]["speed"];
    windSpeed = _t.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // extendBodyBehindAppBar: true,
      // appBar: PreferredSize(
      //   child: ClipRRect(
      //     child: BackdropFilter(
      //       filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
      //       child: AppBar(
      //         backgroundColor: Colors.black.withOpacity(0.2),
      //         elevation: 0.0,
      //       ),
      //     ),
      //   ),
      //   preferredSize: Size(
      //     double.infinity,
      //     2.0,
      //   ),
      // ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('images/background.jpg'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
                Colors.white.withOpacity(0.8), BlendMode.dstATop),
          ),
        ),
        constraints: BoxConstraints.expand(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ClipRRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 5.0,
                  sigmaY: 5.0,
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.33,
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.2),
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.grey.withOpacity(0.2),
                        width: 1.5,
                      ),
                    ),
                  ),
                  child: SafeArea(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 20.0,
                        ),
                        Icon(
                          weatherIcon,
                          color: Colors.white,
                          size: 100.0,
                        ),
                        SizedBox(
                          width: 30.0,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 25,
                            ),
                            Text(
                              "$temperature°C",
                              style: TextStyle(
                                fontSize: 48,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Text(
                              " ${titleCase(description)}",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            Text(
                              " $cityName",
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Column(
              children: [
                MidCard(
                  label: "Pressure",
                  value: "${pressure} hPa",
                ),
                MidCard(
                  label: "Humidity",
                  value: "${humidity}%",
                ),
                MidCard(
                  label: "Minimum",
                  value: "${minTemp}°C",
                ),
                MidCard(
                  label: "Maximum",
                  value: "${maxTemp}°C",
                ),
                MidCard(
                  label: "Wind Speed",
                  value: "${windSpeed} m/s",
                ),
              ],
            ),
            ClipRRect(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 5.0,
                  sigmaY: 5.0,
                ),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.1,
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.2),
                    border: Border(
                      top: BorderSide(
                        color: Colors.grey.withOpacity(0.2),
                        width: 1.5,
                      ),
                    ),
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      FlatButton(
                        onPressed: () async {
                          dynamic weatherData =
                              await weather.getLocationWeather();
                          setState(() {
                            updateUI(weatherData);
                          });
                        },
                        child: Icon(
                          Icons.near_me_sharp,
                          color: Colors.white,
                          size: 32,
                        ),
                      ),
                      FlatButton(
                        onPressed: () async {
                          var typedName = await Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return CityScreen();
                          }));
                          if (typedName != null) {
                            dynamic weatherData =
                                await weather.getCityWeather(typedName);
                            setState(() {
                              updateUI(weatherData);
                            });
                          }
                        },
                        child: Icon(
                          Icons.search,
                          color: Colors.white,
                          size: 32,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MidCard extends StatelessWidget {
  const MidCard({@required this.label, @required this.value});

  final String label;
  final String value;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 5.0,
          sigmaY: 5.0,
        ),
        child: Container(
          margin: EdgeInsets.only(left: 50, right: 50, top: 5, bottom: 5),
          padding: EdgeInsets.only(left: 50, right: 50),
          height: 50,
          decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.2),
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
              color: Colors.black.withOpacity(0.2),
              width: 1.5,
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(label),
              Text(value),
            ],
          ),
        ),
      ),
    );
  }
}
